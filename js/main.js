"use strict";

$(document).ready(function(){

    tablaPersonas = $("#tablaPersonas").DataTable({
       "columnDefs":[
           {"targets": -1, "data":null, "className": "dt-center", "defaultContent": "<div class='text-center'><button class='btn btn-primary mr-2 btnEditar'>Editar</button><button class='btn btn-danger btnBorrar'>Borrar</button></div></div>"},
           {"targets": [0, 1, 2, 3], "className": "text-center"}

    ],
        
        //Para cambiar el lenguaje a español
    "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast":"Último",
                "sNext":"Siguiente",
                "sPrevious": "Anterior"
             },
             "sProcessing":"Procesando...",
        }
    });
    
    let id;
    let opcion;


    $("#btnNuevo").click(() => {
        $("#formPersonas").trigger("reset");
        $(".modal-header").css("background-color", "#28a745");
        $(".modal-header").css("color", "white");
        $(".modal-title").text("Nueva Persona");            
        $("#modalCRUD").modal("show");
        $("#btnGuardar").attr("disabled", true);
        
        function inputInfo() {
            let nombre = $("#nombre").val();

            if (nombre == "") {
                console.log('vacio');
            }
        }

        inputInfo();

        id = null;

        // Alta
        opcion = 1;

        
        let forms = document.getElementsByClassName("needs-validation");
        let validation = Array.prototype.filter.call(forms, (form) => {
            form.addEventListener('submit', (event) => {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                $("#btnGuardar").attr("disabled", false);
            }
            form.classList.add('was-validated');
            });
            console.log(form.checkValidity());
        });
    
    

    });    
    
    // Capturar la fila para editar o borrar el registro
    let fila;
    
    // Boton Editar 
    $(document).on("click", ".btnEditar", function() {
        fila = $(this).closest("tr");
        id = parseInt(fila.find('td:eq(0)').text());
        nombre = fila.find('td:eq(1)').text();
        pais = fila.find('td:eq(2)').text();
        edad = parseInt(fila.find('td:eq(3)').text());
        
        $("#nombre").val(nombre);
        $("#pais").val(pais);
        $("#edad").val(edad);
        
        // Editar
        opcion = 2;
        
        $(".modal-header").css("background-color", "#007bff");
        $(".modal-header").css("color", "white");
        $(".modal-title").text("Editar Persona");            
        $("#modalCRUD").modal("show");  
        
    });

    //botón BORRAR
    $(document).on("click", ".btnBorrar", function() {    
        fila = $(this);
        id = parseInt($(this).closest("tr").find('td:eq(0)').text());
        
        // Eliminar
        opcion = 3;

        let respuesta = confirm(`¿Está seguro de eliminar el registro ${id}?`);
        if(respuesta){
            $.ajax({
                url: "bd/crud.php",
                type: "POST",
                dataType: "json",
                data: {
                    opcion, id
                },
                success: () => {
                    tablaPersonas.row(fila.parents('tr')).remove().draw();
                }
            });
        }   
    });
    
$("#formPersonas").submit((event) => {
    event.preventDefault();    
    nombre = $.trim($("#nombre").val());
    pais = $.trim($("#pais").val());
    edad = $.trim($("#edad").val());    


      //Función para comprobar los campos de texto
   
    //   function checkCampos(inputs) {
    //     let camposRellenados = true;
    //     inputs.find("input").each(() => {
    //     var $this = $(this);
    //         if( $this.val().length <= 0 ) {
    //             camposRellenados = false;
    //             return false;
    //         }
    //     });
    //     if (camposRellenados === false) {
    //         return false;
    //     } else {
    //         return true;
    //     }
    // }

     //Siempre que salgamos de un campo de texto, se chequeará esta función
     $("#formPersonas input").keyup(() => {
        let form = $(this).parents("#formPersonas");
        let check = checkCampos(form);
        if (check) {
            $("#btnGuardar").prop("disabled", false);
        } else {
            $("#btnGuardar").prop("disabled", true);
        }
    });

    

  



    $.ajax({
        url: "bd/crud.php",
        type: "POST",
        dataType: "json",
        data: {
            nombre, pais, edad, id, opcion
        },
        success: (data) => {  
            console.log(data);
            id = data[0].id;            
            nombre = data[0].nombre;
            pais = data[0].pais;
            edad = data[0].edad;
            if (opcion == 1) {
                tablaPersonas.row.add([id,nombre,pais,edad]).draw();
            } else {
                tablaPersonas.row(fila).data([id,nombre,pais,edad]).draw();
            }            
        }        
    });
    $("#modalCRUD").modal("hide");    
    
});    
    
});