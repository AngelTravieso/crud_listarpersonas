<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

// Recepción de los datos enviados mediante POST desde el JS   
$nombre = (isset($_POST['nombre'])) ? $_POST['nombre'] : '';
$pais = (isset($_POST['pais'])) ? $_POST['pais'] : '';
$edad = (isset($_POST['edad'])) ? $_POST['edad'] : '';
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$id = (isset($_POST['id'])) ? $_POST['id'] : '';

switch ($opcion) {
    // Nuevo Usuario
    case 1:
        $consulta = sprintf("INSERT INTO personas (nombre, pais, edad) VALUES('%s', '%s', '%s')", $nombre, $pais, $edad);			
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute(); 

        $consulta = "SELECT id, nombre, pais, edad FROM personas ORDER BY id DESC LIMIT 1";
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data=$resultado -> fetchAll(PDO::FETCH_ASSOC);
        break;
        // Editar
    case 2:
        $consulta = sprintf("UPDATE personas SET nombre='%s', pais='%s', edad='%s' WHERE id='%s'", $nombre, $pais, $edad, $id);		
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();        
        
        $consulta = "SELECT id, nombre, pais, edad FROM personas WHERE id='$id'";       
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data = $resultado -> fetchAll(PDO::FETCH_ASSOC);
        break;        
    case 3://baja
        $consulta = "DELETE FROM personas WHERE id='$id' ";		
        $resultado = $conexion->prepare($consulta);
        $resultado -> execute();
        
        $consulta = "SELECT id, nombre, pais, edad FROM personas WHERE id='$id' ";       
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data = $resultado -> fetchAll(PDO::FETCH_ASSOC);
        break;  
}

print json_encode($data, JSON_UNESCAPED_UNICODE); //enviar el array final en formato json a JS
$conexion = NULL;
